import { TreeNodes, TreeNode, nodes, edges, layouts, configs } from "../data";
import { childHasMultipleConnectedDads } from "./childDadConnection";
import { addToEventLog } from "./eventLog";

export const changeActivity = (nodeObj: TreeNode, isTreeNotFull: boolean) => {
  
    nodeObj.children.forEach((childId) => {
      const edge = Object.values(edges).filter(
        (edge) => edge.target === childId && edge.source === nodeObj.id
      )[0];
      const child = nodes[childId as keyof typeof nodes];
      
      if (isTreeNotFull) {
        if(!child.active){
          addToEventLog(child.id, `open`);
        }
  
        if(!edge.active){
          addToEventLog(edge.id, `${edge.source}-${edge.target} open`);
        }
  
        child.active = true;
        edge.active = true;
      } else {
        const thereIsMoreThenOneActiveDad: boolean =
          childHasMultipleConnectedDads(childId);
        child.active = thereIsMoreThenOneActiveDad;
        edge.active = false;
        
        if (!child.active) {
          addToEventLog(child.id, `close`);
        } else {
          addToEventLog(edge.id, `${edge.source}-${edge.target} close`);
        }
      }
  
      changeActivity(child, isTreeNotFull);
    });
  };
  
  