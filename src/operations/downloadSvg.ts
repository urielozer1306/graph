import * as vNG from "v-network-graph";

export const downloadAsSvg = async (graph:vNG.Instance) => {
    if (!graph) return;
    const text = await graph.exportAsSvgText();
    const url = URL.createObjectURL(new Blob([text], { type: "octet/stream" }));
    const a = document.createElement("a");
    a.href = url;
    a.download = "network-graph.svg"; // filename to download
    a.click();
    window.URL.revokeObjectURL(url);
  }