import { TreeNodes, TreeNode, nodes, edges, layouts, configs } from "../data";

export const computeEdges = () => {
  const n: any = {};

  Object.values(edges).forEach((edge) => {
    if (edge.active) {
      n[edge.id] = edge;
    }
  });

  return n;
};

export const computeNodes = () => {
  const n: TreeNodes = {};
  walkExpandedNodes(nodes["node1"], (node) => (n[node.id] = node));
  
  return n;
};

const walkExpandedNodes = (node: TreeNode, cb: (node: TreeNode) => void) => {
  if (node.active) {
    cb(node);

    node.children.forEach((childId) => {
      walkExpandedNodes(nodes[childId as keyof typeof nodes], cb);
    });
  }
};
