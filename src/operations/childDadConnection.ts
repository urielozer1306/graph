import { TreeNodes, TreeNode, nodes, edges, layouts, configs } from "../data";

export const isTreeNodeNotFull = (node: TreeNode) => {
  if(!isNodeConnectedToAllChildren(node)){
    return true
  }

  for (let childId in node.children) {
    const child = nodes[node.children[childId] as keyof typeof nodes];
    if (isTreeNodeNotFull(child)) {
      return true;
    }
  }

  return false;
};

const isNodeConnectedToAllChildren = (node: TreeNode) => {
  const edgesFromNode= nodeEdgesToChildren(node.id);

  return edgesFromNode.length == node.children.length;
};

export const childHasMultipleConnectedDads = (childId: string) => {
  const connectedDads: TreeNode[] = DadsConnectedToChild(childId);

  return connectedDads.length >= 2;
};

const DadsConnectedToChild = (childId: string) => {
  const connectedDads: TreeNode[] = Object.values(edges)
    .filter((edge) => edge.target === childId && edge.active)
    .map((edge) => nodes[edge.source as keyof typeof nodes]);
  console.log("child: " + childId);
  return connectedDads;
};

export const changeEdgesWidth = (node: TreeNode, newWidth:number) =>{
  const edgesFromNode= nodeEdgesToChildren(node.id)
  edgesFromNode.map(edge => edge.width = newWidth )

  for (let childId in node.children) {
    const child = nodes[node.children[childId] as keyof typeof nodes];
    changeEdgesWidth(child,newWidth)
  }

}
const nodeEdgesToChildren = (nodeId: string) => {
  const edgesFromNode= Object.values(edges)
    .filter((edge) => edge.source === nodeId && edge.active)

    return edgesFromNode;
};