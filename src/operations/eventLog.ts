const EVENTS_COUNT = 7;
export let eventLogs: [string, string, string][] = [];

export const addToEventLog = (node: string, event: string) => {
  const timestamp = new Date().toLocaleString("en-GB");

  eventLogs.unshift([`${node}`, event, `${timestamp}`]);
  eventLogs = eventLogs.slice(0, EVENTS_COUNT);
};
