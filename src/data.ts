import * as vNG from "v-network-graph";

export interface TreeNode extends vNG.Node {
  id: string;
  name: string;
  active: boolean;
  children: string[];
}

export interface Edge extends vNG.Edge {
  id: string;
  source: string;
  target: string;
  active: boolean;
  width: number;
}

export type TreeNodes = Record<string, TreeNode>;
export type Edges = Record<string, Edge>;

export const nodes: TreeNodes = {
  node1: {
    id: "node1",
    name: "node1",
    active: true,
    children: ["node2", "node3", "node8"],
  },
  node2: {
    id: "node2",
    name: "node2",
    active: true,
    children: ["node5", "node4"],
  },
  node3: {
    id: "node3",
    name: "node3",
    active: true,
    children: ["node5", "node6"],
  },
  node4: {
    id: "node4",
    name: "node4",
    active: true,
    children: ["node9", "node10"],
  },
  node5: {
    id: "node5",
    name: "node5",
    active: true,
    children: ["node7", "node9"],
  },
  node6: { id: "node6", name: "node6", active: true, children: ["node7"] },
  node7: { id: "node7", name: "node7", active: true, children: ["node9"] },
  node8: { id: "node8", name: "node8", active: true, children: ["node7"] },
  node9: { id: "node9", name: "node9", active: true, children: ["node11"] },
  node10: { id: "node10", name: "node10", active: true, children: [] },
  node11: { id: "node11", name: "node11", active: true, children: [] },
};

const edgesDefault: Pick<Edge, "active" | "width"> = {
  active: true,
  width: 1,
};
let edgesCounter = 0;

const generateEdges = (): Edges => {
  const tempEdges: Edges = {};
  Object.values(nodes).forEach((node) =>
    node.children.forEach((child) => addEdge(node.id, child, tempEdges))
  );

  return tempEdges;
};

const addEdge = (sourceId: string, targetId: string, tempEdges: Edges) => {
  tempEdges[`edge${edgesCounter}`] = {
    id: `edge${edgesCounter}`,
    source: sourceId,
    target: targetId,
    ...edgesDefault,
  };
  edgesCounter++;
};

export const edges: Edges = generateEdges();

/*export const edges: Edges = {
    edge1: { id:"edge1",source: "node1", target: "node2" ,...edgesDefault},
    edge2: { id:"edge2",source: "node1", target: "node3" ,...edgesDefault},
    edge3: { id:"edge3",source: "node2", target: "node4" ,...edgesDefault},
    edge4: { id:"edge4",source: "node3", target: "node6" ,...edgesDefault},
    edge5: { id:"edge5",source: "node2", target: "node5" ,...edgesDefault},
    edge6: { id:"edge6",source: "node3", target: "node5",...edgesDefault},
    edge7: { id:"edge7",source: "node6", target: "node7" ,...edgesDefault},
    edge8: { id:"edge8",source: "node5", target: "node7" ,...edgesDefault},
    edge9: {id:"edge9", source: "node1", target: "node8" ,...edgesDefault},
    edge10: {id:"edge10", source: "node8", target: "node7" ,...edgesDefault},
  };*/

export const layouts = {
  nodes: {
    node1: { x: 0, y: 0 },
    node2: { x: -80, y: 80 },
    node3: { x: 80, y: 80 },
    node4: { x: -160, y: 160 },
    node5: { x: 0, y: 160 },
    node6: { x: 160, y: 160 },
    node7: { x: 240, y: 240 },
    node8: { x: 240, y: 80 },
    node9: { x: 80, y: 320 },
    node10: { x: -160, y: 240 },
    node11: { x: 80, y: 400 },
  },
};

export const configs = vNG.defineConfigs<vNG.Node>({
  node: {
    selectable: true,
    label: {
      visible: true,
      fontFamily: undefined,
      fontSize: 11,
      lineHeight: 1.1,
      color: "#000000",
      margin: 4,
      direction: "south",
      text: "name",
    },
    focusring: {
      color: "yellow",
    },
  },
  edge: {
    normal: {
      width: (edge) => edge.width, // Use the value of each edge object
      // color: edge => edge.color,
      // dasharray: edge => (edge.dashed ? "4" : "0"),
    },
  },
});
